package mqttsqueezebox

import io.circe.Decoder

case class Message(message: String, duration: Option[Int] = None)

object Message {

  import io.circe.generic.semiauto._

  implicit val messageDecoder: Decoder[Message] = {
    val objectDecoder: Decoder[Message] = deriveDecoder[Message]
    val stringDecoder: Decoder[Message] = Decoder.decodeString.map { message =>
      Message(message)
    }
    objectDecoder.or(stringDecoder)
  }

}
