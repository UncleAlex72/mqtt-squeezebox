package mqttsqueezebox

import akka.actor.ActorSystem
import akka.stream.alpakka.mqtt.scaladsl.MqttSource
import akka.stream.alpakka.mqtt.{
  MqttConnectionSettings,
  MqttMessage,
  MqttQoS,
  MqttSubscriptions
}
import akka.stream.scaladsl.{Flow, Source}
import akka.{Done, NotUsed}
import com.typesafe.scalalogging.StrictLogging
import io.circe.parser._
import mqttsqueezebox.config.MqttSqueezeboxConfiguration
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence
import pureconfig.ConfigSource
import uk.co.unclealex.squeezebox.{
  AkkaStreamMediaServerClient,
  MediaServer,
  MediaServerClient,
  SqueezeboxProgram
}

import scala.concurrent.duration.DurationInt
import scala.concurrent.{ExecutionContext, Future}

object Application extends App with StrictLogging {

  val configurationFile = Option(System.getenv("MQTT_SQUEEZEBOX_CONF_FILE"))
    .getOrElse("/etc/mqtt-squeezebox/mqtt-squeezebox.conf")

  val mqttSqueezeboxConfiguration =
    ConfigSource
      .file(configurationFile)
      .load[MqttSqueezeboxConfiguration] match {
      case Right(mqttSqueezeboxConfiguration: MqttSqueezeboxConfiguration) =>
        mqttSqueezeboxConfiguration
      case Left(configReaderFailures) =>
        logger.error(
          s"Configuration parsing failed:\n${configReaderFailures.prettyPrint(2)}"
        )
        throw new IllegalStateException("Configuration parsing failed.")
    }

  implicit val actorSystem: ActorSystem = ActorSystem()
  implicit val ec: ExecutionContext = actorSystem.dispatcher

  val mqttSource: Source[MqttMessage, Future[Done]] = {
    val mqttConfiguration = mqttSqueezeboxConfiguration.mqtt
    MqttSource.atMostOnce(
      MqttConnectionSettings(
        mqttConfiguration.url,
        mqttConfiguration.clientId,
        new MemoryPersistence
      ).withAutomaticReconnect(true),
      MqttSubscriptions(mqttConfiguration.topic, MqttQoS.AtLeastOnce),
      bufferSize = 8
    )
  }

  val squeezebox = mqttSqueezeboxConfiguration.squeezebox
  val mediaServer = MediaServer(squeezebox.host, squeezebox.port)

  val mediaServerClient: MediaServerClient = new AkkaStreamMediaServerClient(
    mediaServer
  )

  val messageSource = mqttSource.map(_.payload.utf8String).flatMapConcat {
    payload =>
      val messages = for {
        json <- parse(payload).toSeq
        message <- json.as[Message].toSeq
      } yield {
        message
      }
      Source(messages)
  }

  val squeezeboxFlow: Flow[Message, String, NotUsed] =
    Flow[Message].flatMapConcat { message =>
      Source.future {
        val duration = message.duration
          .map(_.seconds)
          .getOrElse(squeezebox.defaultMessageDuration)
        val msg = message.message
        mediaServerClient
          .execute {
            SqueezeboxProgram.forEachPlayer { player =>
              SqueezeboxProgram.displayMessage(
                player.playerId,
                msg,
                msg,
                duration
              )
            }
          }
          .map { _ =>
            s"""Displaying "$msg" for $duration """
          }
      }
    }

  messageSource.via(squeezeboxFlow).runForeach(msg => logger.info(msg))
}
