package mqttsqueezebox.config

import pureconfig._
import pureconfig.generic.semiauto._

import scala.concurrent.duration.FiniteDuration

case class MqttConfiguration(url: String, clientId: String, topic: String)

case class SqueezeboxConfiguration(
    host: String,
    port: Int,
    defaultMessageDuration: FiniteDuration
)

case class MqttSqueezeboxConfiguration(
    mqtt: MqttConfiguration,
    squeezebox: SqueezeboxConfiguration
)

object MqttSqueezeboxConfiguration {

  implicit val mqttConfigurationReader: ConfigReader[MqttConfiguration] =
    deriveReader[MqttConfiguration]
  implicit val squeezeboxConfigurationReader
      : ConfigReader[SqueezeboxConfiguration] =
    deriveReader[SqueezeboxConfiguration]
  implicit val mqttSqueezeboxConfigurationReader
      : ConfigReader[MqttSqueezeboxConfiguration] =
    deriveReader[MqttSqueezeboxConfiguration]

}
