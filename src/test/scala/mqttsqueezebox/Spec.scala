package mqttsqueezebox

import akka.actor.ActorSystem
import akka.testkit.TestKit
import org.scalatest.BeforeAndAfterAll
import org.scalatest.flatspec.AsyncFlatSpecLike
import org.scalatest.matchers.should.Matchers

abstract class Spec(name: String)
    extends TestKit(ActorSystem(name))
    with AsyncFlatSpecLike
    with Matchers
    with BeforeAndAfterAll {

  final override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

}
