import sbt.Keys._
import sbt._
import sbtrelease.ReleasePlugin.autoImport.ReleaseTransformations._

lazy val `mqtt-squeezebox` =
  project
    .in(file("."))
    .enablePlugins(
      DockerPlugin,
      AshScriptPlugin
    )
    .settings(
      scalaVersion := "2.13.2",
      organization := "uk.co.unclealex",
      scalacOptions += "-Ymacro-annotations",
      libraryDependencies ++= Seq(
        "io.circe" %% "circe-core",
        "io.circe" %% "circe-generic",
        "io.circe" %% "circe-parser"
      ).map(_ % "0.14.1"),
      libraryDependencies ++= Seq(
        "org.typelevel" %% "cats-core" % "2.3.0",
        "com.github.pureconfig" %% "pureconfig" % "0.15.0",
        "uk.co.unclealex" %% "futures" % "1.0.1",
        "com.typesafe.scala-logging" %% "scala-logging" % "3.9.3",
        "com.lightbend.akka" %% "akka-stream-alpakka-mqtt" % "3.0.0",
        "ch.qos.logback" % "logback-classic" % "1.2.3",
        "com.typesafe.akka" %% "akka-stream-testkit" % "2.6.14" % Test,
        "org.scalatest" %% "scalatest" % "3.2.9" % Test,
        "uk.co.unclealex" %% "akka-logitechmediaserver" % "1.0.4"
      ),
      // Docker
      dockerBaseImage := "adoptopenjdk/openjdk11-openj9:alpine",
      maintainer := "Alex Jones <alex.jones@unclealex.co.uk>",
      dockerRepository := Some("unclealex72"),
      packageName := "mqtt-squeezebox",
      dockerUpdateLatest := true,
      javaOptions in Universal += "-Dpidfile.path=/dev/null",
      Global / onChangedBuildSource := ReloadOnSourceChanges,
      ivyLoggingLevel := UpdateLogging.Quiet,
      scalacOptions += "-Ymacro-annotations",
      releaseProcess := Seq[ReleaseStep](
        releaseStepCommand("scalafmtCheckAll"),
        releaseStepCommand("scalafmtSbtCheck"),
        inquireVersions, // : ReleaseStep
        runTest, // : ReleaseStep
        setReleaseVersion, // : ReleaseStep
        commitReleaseVersion, // : ReleaseStep, performs the initial git checks
        tagRelease, // : ReleaseStep
        releaseStepCommand(
          "packageBin"
        ), // : ReleaseStep, build server docker image
        releaseStepCommand(
          "docker:publish"
        ), // : ReleaseStep, build server docker image
        setNextVersion, // : ReleaseStep
        commitNextVersion, // : ReleaseStep
        pushChanges // : ReleaseStep, also checks that an upstream branch is properly configured
      )
    )
